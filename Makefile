#!/usr/bin/env bash

# List files to be made by finding all *.asciidoc files and appending .pdf
PDFS := $(patsubst %.asciidoc,%.pdf,$(wildcard *.asciidoc))
# List files to be made by finding all *.asciidoc files and appending .html
HTMLS := $(patsubst %.asciidoc,%.html,$(wildcard *.asciidoc))

.PHONY: build
build: $(PDFS) $(HTMLS) ## Makes all the PDF and HTML files listed

# This generic rule accepts PDF targets with corresponding Asciidoc
# source, and makes them using asciidoctor-pdf
%.pdf: %.asciidoc
	asciidoctor -r asciidoctor-pdf -b pdf $< -D public -o $@

# This generic rule accepts HTML targets with corresponding Asciidoc
# source, and makes them using asciidoctor
%.html: %.asciidoc
	asciidoctor $< -D public -o index.html

.PHONY: clean
clean: ## Remove all PDF and HTML outputs
	rm -rf public

all: clean build ## Remove all PDF and HTML outputs then build them again (Default)

.PHONY: help
help: ## displays the description of each target
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := all