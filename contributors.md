# Contributors

![smur's avatar](https://framagit.org/uploads/-/system/user/avatar/42128/avatar.png)

[**Samuel Marcaille**](https://framagit.org/smur)

[💻](https://framagit.org/smur/gb-faq/commits?author=smur "Code") [🎨](https://framagit.org/smur/gb-faq "Design") [📖](https://framagit.org/smur/gb-faq "Documentation")
